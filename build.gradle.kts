
plugins {
    `maven-publish`
    `version-catalog`
    signing
}

group = "com.andsn.catalog"
version = "1.1-SNAPSHOT"

val signingProp = java.util.Properties().apply {
    load(rootProject.file("signing.properties").inputStream())
}

catalog {
    // declare the aliases, bundles and versions in this block
    versionCatalog {
       // library("my-lib", "com.mycompany:mylib:1.2")
          from(files("./gradle/libs.version.toml"))
    }
}

repositories {
    mavenCentral()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = "catalog"
            from(components["versionCatalog"])
            description = "manage android dependencies"
        }
    }

    repositories{
        mavenLocal()
        maven {
            name = "mavenCentral"
            val url = if (version.toString().endsWith("SNAPSHOT")) {
                "https://s01.oss.sonatype.org/content/repositories/snapshots/"
            } else {
                "https://s01.oss.sonatype.org/content/repositories/releases/"
            }
            setUrl(url)
            credentials {
                username = signingProp.getProperty("mavenCentralUsername")
                password = signingProp.getProperty("mavenCentralPassword")
            }
        }
    }
}
ext.set("signing.keyId", signingProp.getProperty("signing.keyId"))
ext.set("signing.password", signingProp.getProperty("signing.password"))
ext.set("signing.secretKeyRingFile", signingProp.getProperty("signing.secretKeyRingFile"))

signing {
    sign(publishing.publications)
}